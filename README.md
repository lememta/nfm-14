Testing-based compiler validation for Lustre programs compiled to C.


Contents:

* tools/lustrec :: Modular compiler from Lustre to C. and Mutation generator for Lustre programs
* tools/pkind-tg ::  Pkind version for test generation. 

* benchmarks :: Lustre benchmarks used in the experimental evaluation.

* results :: The results of the exeprimental evaluation.


Compile:

Needed binaries: 
- lus2ec from Verimag lustre-v4 distribution
- parallel (GNU)


** lustrec and lustrem
cd tools/lustrec
./configure; make; sudo make install

** pkind-tg
cd tools/pking-tg
after solving dependencies:
autoconf; ./configure; make; sudo make install

** scripts:
cd tools/scripts:
./gen_test_suite.sh `pwd`/../lustrec/test/src/kind_fmcad08/misc/two_counters.lus
-a
be careful, you must provide an absolute path for the script to work
Results can be read in folder "summaries"

