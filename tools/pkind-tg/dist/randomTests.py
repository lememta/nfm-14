from LogManager import LoggingManager
import json
from nodeDecomposer import NodeComposer
from lustreParser import LParser
import random
from random import randint
from kindInterface import KindUtil
import os
from ccNodes import CcNodes
from pyparsing import ParseException
import xml.etree.ElementTree as ET


class RandomTestGeneration(object):
    def __init__(self):
        self._log = LoggingManager.get_logger(__name__)
        self.composer = NodeComposer()
        return


    def parseFile(self, lusFile):
        self._log.info("Parsing Lustre file: %s" % lusFile)
        parser = LParser()
        with open(lusFile, 'r') as f:
            try:
                self.LustreAST= parser.parse(f.read())
                self._log.debug(parser.ppAST(self.LustreAST))
                return True
            except ParseException, err:
                self._log.exception(str(err))
                return False


    def compileAndRun(self, lusFile, depth, node):
        kind = KindUtil()
        try:
            result, cFile = kind.runCompiler(lusFile, node)
            if "C code generation" not in result:
                self._log.error("Error compiling" + str(lusFile))
            else:
                self._log.info("C file generated: " + str(cFile))
                tests = self.randomTests(lusFile,depth)
                d = os.path.dirname(lusFile)
                result, outFile = kind.runCCompiler(d, cFile)
                if result == "" and outFile:
                    self._log.info("Successfully compiled: " + outFile)
                    self.runTestCases(tests, outFile)
                else:
                    self._log.error("Compile")
        except Exception as e:
            self._log.exception(str(e))

    def runTestCases(self, tests, prog):
        self._log.info("Running test cases for " + str(prog))
        kind = KindUtil()
        try:
            for k,s in tests.iteritems():
                testResult = kind.runTestCases(s, prog)
                print testResult
        except Exception as e:
            print str(e)


    def randomTests(self, lusFile, depth):
        inputVars = []
        all_tests = {}
        if self.parseFile(lusFile):
            self._log.info("Successful parse")
            nodeNames = self.LustreAST.keys()
            for n,d in self.LustreAST.iteritems():
                if n == "glob":
                    pass
                else:
                    inputVars = d["input_vars"]
            for i in range(1, depth):
                test = self.randomize(inputVars, depth)
                all_tests.update({i: test})
            return all_tests
        else:
            self._log.error("Parsing error")
            return None


    def randomize(self, inputVars, depth):
        test_i = ""
        for var in inputVars:
            if var[1] == "bool":
                randBool = self.randBool()
                test_i += str(randBool) + " "
            elif var[1] == "int":
                randInt = self.randInt()
                test_i += str(randInt) + " "
            elif var[1] == "real":
                randReal, randReal1 = self.randReal()
                test_i += (str(randReal) + "." + str(randReal1) + " ")
        return test_i


    def randBool(self):
        return bool(random.getrandbits(1))


    def randInt(self):
        return randint(0,50)


    def randReal(self):
        return randint(0,50), randint(0,10)


    def storeNodes(self, lusFile, nodeWithCondition):
        self._log.debug("Storing Nodes")
        f_node = lusFile+".tg"
        f = open(f_node, "w+")
        f.write(nodeWithCondition)
        f.close
        return f_node

        for node_name, node in lustreNodes.iteritems():
            if node and anyPropNodes[node_name]:
                f_node = self.d+os.sep+node_name+".lus"
                allNodes.append(f_node)
                compProps.update({f_node: componentProps[node_name]})
                f = open(f_node, "w+")
                f.write(node)
                f.close
        return allNodes, compProps


    def generateTestCaseCondition(self, lusFile, cond):
        inputVars = []
        all_tests = {}
        nodeWithCondition = None
        kind = KindUtil()
        if self.parseFile(lusFile):
            self._log.info("Successful parse")
            nodeNames = self.LustreAST.keys()
            mk_node = CcNodes(nodeNames)
            for n,d in self.LustreAST.iteritems():
                if n == "glob":
                    pass
                else:
                    nodeWithCondition, _, _, _ =  mk_node.tgCond(d, cond)
            f_node = self.storeNodes(lusFile, nodeWithCondition)
            print kind.runTestGen(f_node)
        else:
            self._log.error("Parsing")


    def generateAndRun(self, lusFile, cond):
        self._log.info("Generating and running test cases")
        progWithTrapProp = self.generateTestCaseConditionFile(lusFile, cond)
        kind = KindUtil()
        if progWithTrapProp:
            for prog in progWithTrapProp:
                testXML = kind.runTestGen(prog)
                testOracle = self.getTestOracle(testXML)

    def generateTestCaseConditionFile(self, lusFile, condFile):
        inputVars = []
        all_tests = {}
        nodeWithCondition = None
        kind = KindUtil()
        progWithTrapProp = []
        if self.parseFile(lusFile):
            self._log.info("Successful parse")
            nodeNames = self.LustreAST.keys()
            mk_node = CcNodes(nodeNames)
            for n,d in self.LustreAST.iteritems():
                if n == "glob":
                    pass
                else:
                    inp = d["input_vars"]
                    out = d["output_vars"]
                    f_cond = open(condFile, "r")
                    outCond, inCond, inpVar, outVar = self.collectInputOutput(inp, out)
                    c_i = 1
                    for c in f_cond.readlines():
                        cl = c.rstrip("\n")
                        newCond = self.makeCondition(cl, inCond, outCond, inpVar, outVar)
                        print newCond
                        nodeWithCondition, _, _, _ =  mk_node.tgCond(d, newCond)
                        f_node = self.storeNodes((lusFile+"_"+(str(c_i))), nodeWithCondition)
                        c_i +=1
                        progWithTrapProp.append(f_node)
            return progWithTrapProp
        else:
            self._log.error("Parsing")
            return None

    def collectInputOutput(self, inputs, outputs):
        condBool = [inp[0] for inp in inputs if inp[1] == 'bool'] + [out[0] for out in outputs if out[1] == 'bool']
        condInt = [inp[0] for inp in inputs if inp[1] == 'int'] + [out[0] for out in outputs if out[1] == 'int']
        print condInt
        print condBool
        b = " and ".join(x for x in condBool)
        i = " and ".join(("("+x+"= 0) or not("+x+" = 0)") for x in condInt)
        return "( "+ b + ")", "( "+ i + ")", condInt, condBool


    def makeCondition(self, cond, inCond, outCond, inpVar, outVar):
        adjustedcond = []
        allVar = inpVar + outVar
        allGood = False
        for i in allVar:
            if i not in cond:
                c = cond + " and " + inCond + " and " + outCond
                return c
            else:
                allGood = True
        if allGood:
            return cond


    def getTestOracle(self, testCase):
        self._log.info("Getting test oracle")
        bench_out = {}
        try:
            xmldoc = ET.fromstring(r)
            properties_tag = (xmldoc[0]).attrib['name']
            print "PROPERTIES:\n"
            props = self.componentProps[node_name]
            prop_formula = "flattened formula (see lustre file)"
            for p in properties_tag.split():
                try:
                    prop_formula = props[p]
                except:
                    pass
                result = "\t\t Result: " + xmldoc[0][3].text + "\n"\
                         + "\t\t K: " +  xmldoc[0][2].text + "\n"\
                         + "\t\t TIME: " + xmldoc[0][1].text + "\n"
                print "\t" + p + " : " +  prop_formula + "\n" + result
                print "\t ---------------"
        except Exception as e:
            print e
            print "\t Error in verification"
            pass
