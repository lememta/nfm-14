
(** Process scheduler *)

(** 
@author Temesghen Kahsai

*)

open Types
open Flags
open Exceptions
open Channels
open Globals
open Mpi



module Dispatch =
struct   
  (** Schedule processes *)
  let schedule filename proc_number = 
    if(proc_rank = base_proc) then (* Base process *)
      ( 
	       Globals.base_time_start := (wtime());
	       Base_proc.main filename;
      )
    else if(proc_rank=step_proc) then (* Inductive step process *)
      (
	     Globals.step_time_start := (wtime());
	       Induct_proc.main filename;
      )
    else if (proc_rank = !kind_ai_proc) then (* Kind_AI process *)
      (
    	 Globals.kind_ai_time_start := (wtime());
    	 Kind_AI_wrapper.main filename
      )
    else if proc_rank = !inv_gen_proc then (* Invariant generation process *)
      (
    	 Globals.inv_gen_time_start := (wtime());
    	 let defdoc,maxdepth,def_hash, pvars = Defgen.start_invariant_generator filename in
    	   if(!Flags.incremental) then (
    	       Incremental_inv_gen.produce_invariants defdoc maxdepth def_hash pvars;
    	   ) else (
    	     Inv_generator.loop defdoc maxdepth def_hash pvars; 
    	   )
      )
    else
      raise (Wrong_number_of_proc (proc_number, proc_rank));

end  
  
  
  










