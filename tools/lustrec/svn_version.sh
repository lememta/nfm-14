#!/bin/bash

version=`svn info | awk -v ver="UNKNOWN" -F ":" '/R?vision/ { ver=$2 } END { print ver }' | tr -d " "`

filename=version.ml

echo "Generating version number in file \"${filename}\" ..."

echo "" > src/${filename}
echo "(* Version file generated by Oasis *)" >> src/${filename}
echo "" >> src/${filename}
echo "let number = \"${version}\"" >> src/${filename}
