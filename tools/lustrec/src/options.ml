(* ----------------------------------------------------------------------------
 * SchedMCore - A MultiCore Scheduling Framework
 * Copyright (C) 2009-2011, ONERA, Toulouse, FRANCE - LIFL, Lille, FRANCE
 *
 * This file is part of Prelude
 *
 * Prelude is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation ; either version 2 of
 * the License, or (at your option) any later version.
 *
 * Prelude is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY ; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program ; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *---------------------------------------------------------------------------- *)

let version = "1.2"
let main_node = ref ""
let static_mem = ref true
let print_types = ref true
let print_clocks = ref true
let delay_calculus = ref true
let track_exceptions = ref true
let ansi = ref false
let check = ref false
let c_spec = ref false
let java = ref false
let dest_dir = ref ""
let verbose_level = ref 1
let nb_mutants = ref 1000
let gen_mcdc = ref false

let common_options =
  [      "-node", Arg.Set_string main_node, "specifies the main node";
"-print_types", Arg.Set print_types, "prints node types";
    "-print_clocks", Arg.Set print_clocks, "prints node clocks";
    "-verbose", Arg.Set_int verbose_level, " changes verbose level <default: 1>";
    "-version", Arg.Unit (fun () -> print_endline version), " displays the version";]

let lustrec_options = 
  [ "-d", Arg.Set_string dest_dir, "produces code in the specified directory";
    "-init", Arg.Set delay_calculus, "performs an initialisation analysis for Lustre nodes";
    "-dynamic", Arg.Clear static_mem, "specifies a dynamic allocation scheme for main Lustre node (default: static)";
    "-ansi", Arg.Set ansi, "specifies that generated C code is ansi C90 compliant (default is C99)";
    "-check-access", Arg.Set check, "checks at runtime that array accesses always lie within bounds (default: no check)";
    "-c-spec", Arg.Set c_spec, 
    "generates a C encoding of the specification instead of ACSL contracts and annotations. Only meaningful for the C backend";
    "-java", Arg.Set java, "generates Java output instead of C";
  ] @ common_options

let lustrem_options =
  [ "-d", Arg.Set_string dest_dir, "produces mutants in the specified directory";
    "-nb", Arg.Set_int nb_mutants, "Number of mutants to produce (default 1000)";
    "-mcdc_cond", Arg.Set gen_mcdc, "Generate MC/DC conditions"
 ]
  @ common_options

(* Local Variables: *)
(* compile-command:"make -C .." *)
(* End: *)
