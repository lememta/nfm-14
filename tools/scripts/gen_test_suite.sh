eval set -- $(getopt -n $0 -o "-ahstbmknv:" -- "$@")

declare h s b m k n a v t
declare -a files

node="top"

mkdir -p tmp
mkdir -p summaries

base_suite () {
  mkdir -p ${conds_dir}
  mkdir -p ${suite_dir}
  echo Generating base test suite: $suite_file
  rm -f ${suite_file}.no_mcdc tmp/tmp_${name}.res
  centralized_file=${conds_dir}/${name}.ec
  lus2ec $file $node -o $centralized_file 
  timeout 10m lustrem -verbose 0 -mcdc_cond -node $node $centralized_file > $cond_file
  while read cond; do
    echo .. $cond 
    cat $centralized_file | grep -v "^--" | xargs | sed "s/tel/tel\n/g" | grep -v "node ${node}" | sed "s/; /;\n/g" > tmp/tmp_${name}
    cat $centralized_file | grep -v "^--" | xargs | sed "s/tel/tel\n/g" | grep "node ${node}" | sed "s/tel/--!PROPERTY: not ($cond); tel/;s/; /;\n/g" >> tmp/tmp_${name}
    timeout $timeout pkind -tg tmp/tmp_${name} | xsltproc input.xsl - 2> /dev/null| tee -a tmp/tmp_${name}.res 
    [ ${PIPESTATUS[1]} -ne 0 ] && echo $cond >> ${suite_file}.no_mcdc
  done < "$cond_file"
  sort -u tmp/tmp_${name}.res > $suite_file
  #rm tmp.res tmp 
}

gen_ref_bin () {
  echo Generating reference binary
  mkdir -p ${ref_bin_dir}
  cd ${ref_bin_dir}
  lustrec -node $node -verbose 0 $file
  make -f $name.makefile > /dev/null
  cd ..
}
    
gen_mutants () {
  echo Generating and compiling mutants
  mkdir -p ${mutants_dir}/${name}
  timeout 10m lustrem -nb 200 -d ${mutants_dir}/${name} -verbose $verbose $file
  cd ${mutants_dir}/${name}
  rm -f ${name}.mutants_compile
  for mutant in ${name}.mutant*.lus; do
      echo "echo Compiling mutant ${mutant} && lustrec -node ${node}_mutant -verbose 0 $mutant && make -f `basename $mutant .lus`.makefile > /dev/null" >> ${name}.mutants_compile
  done
  parallel -P 1 < ${name}.mutants_compile
  cd ../..
}
    
# echo Running test
# while read test; do
#   echo .. test $test >&2 
#   echo $test | sed "s/ /\n/g" > test_tmp
#   ./$binary < test_tmp >> oracle
# done < $suite_file
    
run_mutants () {
  echo Trying to kill mutants: running test on mutants $name
  echo "Summary for file ${file}" > $summary
  echo "# tests: `wc -l ${suite_file}`" >>$summary 
  all_killed=1
# rm -f log_tmp; touch log_tmp
  rm -f tmp/log_tmp_${name}
  for mutant in mutants/${name}/${name}.mutant.*_${node}_mutant; do
    killed=0
    while read test; do
      echo $test | sed "s/ /\n/g" > tmp/test_tmp_${name}
      $binary < tmp/test_tmp_${name} > tmp/oracle_tmp_${name}
      $mutant < tmp/test_tmp_${name} > tmp/run_tmp_${name}
      diff tmp/oracle_tmp_${name} tmp/run_tmp_${name} > /dev/null
      [ $? -eq 1 ] && killed=1
      [ $killed -eq 1 ] && echo $mutant killed by test $test >> tmp/log_tmp_${name} 
      [ $killed -eq 1 ] && [ $verbose -ge 2 ] && echo $mutant killed by test $test  
      [ $killed -eq 1 ] && break
    done < "$suite_file"
    [ $killed -eq 0 ] && all_killed=0 && echo "$mutant unkilled" >> tmp/log_tmp_${name}
  done
  [ $all_killed -eq 1 ] && echo "All mutants killed" >> $summary && echo "" >> $summary && cat tmp/log_tmp_${name} >> $summary
  [ $all_killed -eq 0 ] && echo "`cat tmp/log_tmp_${name} | grep unkilled | wc -l` mutant(s) unkilled" >> $summary && echo "" >> $summary && cat tmp/log_tmp_${name} >> $summary
# rm -f tmp/test_tmp_${name} tmp/oracle_tmp_${name} tmp/run_tmp_${name} tmp/log_tmp_${name}
}

kill_mutants () {
  rm -f tmp/tmp_${name}_new.res ${summary}_new
  unkilleds=`grep ${name} $summary | grep unkilled | sed "s/ .*//"`
  echo unkilleds: $unkilleds
  touch ${summary}_new tmp/tmp_${name}_new.res
  inputs=`cat $file | grep -v "^[' ']*--" | xargs | sed "s/.*node ${node}[' ']*(\([^')']*\))[' ']*returns[' ']*(\([^')']*\))[' ']*;.*/\1/"`
  outputs=`cat $file | grep -v "^[' ']*--" | xargs | sed "s/.*node ${node}[' ']*(\([^')']*\))[' ']*returns[' ']*([' ']*\([^')']*\))[' ']*;.*/\2/;s/ //g"`
  outputs_mutant=`echo $outputs | sed "s/ /_mutant /;s/\([a-zA-Z]\):/\1_mutant:/"`
  echo outputs: /$outputs/ outputs mutant: /$outputs_mutant/
  top_call=`echo $outputs | sed "s/:[^';']*;/,/g;s/:.*//"`
  mutant_call=`echo $outputs_mutant | sed "s/:[^';']*;/,/g;s/:.*//"`
  input_args=`echo $inputs | sed "s/:[^';']*;/,/g;s/:.*//"`
  for unkilled in $unkilleds; do
    mutant=mutants/${name}/`basename $unkilled _${node}_mutant`.lus
    echo "Trying to create a new test to kill $mutant"
    kill_main=mutants/${name}/`basename $unkilled _${node}_mutant`.kill_lus
    cat $file > ${kill_main}
    echo " " >> ${kill_main}
    cat $mutant >> ${kill_main}
    echo "node kill_main ($inputs) returns (kill_the_mutant: bool);" >> ${kill_main}
    echo "var $outputs; $outputs_mutant;" >>  ${kill_main}
    echo "let" >>  ${kill_main}
    echo "  $top_call = ${node} ($input_args);" >>  ${kill_main}
    echo "  $mutant_call = ${node}_mutant ($input_args);" >>  ${kill_main}

    IFS="," 
    tmp_uid=v$(echo `date +%N`)
    tmp_vars=tmp/$tmp_uid
    touch ${tmp_vars} 
    for i in $input_args; do echo "($i or not $i)" > ${tmp_vars}; done
    tautology=`cat $tmp_vars | xargs | sed "s/) (/) and (/g"`
    rm ${tmp_vars}

    echo "  kill_the_mutant = ($top_call = $mutant_call) and ${tautology};" >>  ${kill_main}
    echo "tel" >>  ${kill_main}
    lus2ec ${kill_main} kill_main -o ${kill_main}.flat.ec 
    cat ${kill_main}.flat.ec | xargs | sed "s/tel/--!PROPERTY: kill_the_mutant; tel/;s/;/;\n/g" > ${kill_main}.flat_lus
    test_ok=1
    prove_ok=1
    timeout $timeout pkind -tg ${kill_main}.flat_lus | xsltproc input.xsl - 2> /dev/null| tee -a tmp/tmp_${name}_new.res
    [ ${PIPESTATUS[1]} -eq 0 ] && test_ok=0
    [ $test_ok -eq 0 ] && echo "$mutant killed by new test"  >> ${summary}_new
    [ $test_ok -ne 0 ] && echo "Unable to create a test to kill mutant $mutant" >> ${summary}_new
    [ $test_ok -ne 0 ] && timeout $timeout pkind ${kill_main}.flat_lus 2> /dev/null | grep -q Valid &&  [ ${PIPESTATUS[1]} -eq 0 ] && prove_ok=0
    [ $test_ok -ne 0 ] && [ $prove_ok -eq 0 ] && echo "Unobservable mutation: beware of dead code" >> ${summary}_new
    [ $test_ok -ne 0 ] && [ $prove_ok -ne 0 ] && echo "Inconclusive analysis">> ${summary}_new
  #rm tmp.res tmp                                             
  done
  sort -u tmp/tmp_${name}_new.res > ${suite_file}_new

}

usage () {
  echo "usage: $0 [-hsbmka] [-n nodename] lustre_file"
  echo "-n node: by default node is main"
  echo "-a: perform all steps"
  echo "-s: generate base test suite"
  echo "-b: generate and compile main binary"
  echo "-m: generate and compile mutants"
  echo "-k: try to kill mutants"
  echo "-t: create new tests to kill unkilled mutants"
  echo "-v: verbose level (default 1)"
}

nobehavior=1
verbose=1

while [ $# -gt 0 ] ; do
  case "$1" in
      -v) shift ; verbose="$1"; shift ;;
      -a) nobehavior=0; s=1 ; b=1; m=1; k=1; t=1; shift ;;
      -h) nobehavior=0; h=1 ; shift ;;
      -t) nobehavior=0; t=1 ; shift ;;
      -s) nobehavior=0; s=1 ; shift ;;
      -b) nobehavior=0; b=1 ; shift ;;
      -m) nobehavior=0; m=1 ; shift ;;
      -k) nobehavior=0; k=1 ; shift ;;
      -n) shift ; node="$1" ; shift ;;
      --) shift ;;
      -*) echo "bad option '$1'" ; exit 1 ;;
      *) files=("${files[@]}" "$1") ; shift ;;
  esac
done

file=${files[0]}
ref_bin_dir="./ref_bin"
mutants_dir="./mutants"
conds_dir="./conds"
suite_dir="./tests"
name=`basename $file .lus`
binary=${ref_bin_dir}/${name}_${node}
cond_file=${conds_dir}/$name.conds
suite_file=${suite_dir}/$name.test_suite
summary=summaries/$name.summary
timeout=2

if [ ${#files} -eq 0 ] ; then
  echo input lustre file required
  exit 1
fi

[ ! -z "$h" ] && usage && exit 0
[ ! -z "$s" ] && base_suite 
[ ! -z "$b" ] && gen_ref_bin
[ ! -z "$m" ] && gen_mutants
[ ! -z "$k" ] && run_mutants
[ ! -z "$t" ] && kill_mutants
[ "$nobehavior" -eq 1 ] && echo "Must provide an argument in [sbmkta]" && usage

