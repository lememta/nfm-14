<?xml version="1.0" encoding="ISO-8859-1"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/TR/REC-html40" result-ns="">

<xsl:template match="/">
  <xsl:for-each select="//Value[@type='input']">
    <xsl:sort select="@step"/>
	<xsl:apply-templates/> 
        <xsl:text> </xsl:text>

  </xsl:for-each>
</xsl:template>

</xsl:stylesheet>
