for kind in misc protocol simulation; do
for lus in ../lustrec/test/src/kind_fmcad08/${kind}/*.lus; do
  name=`basename $lus .lus`
  summary=summaries/${name}.summary
#for i in summaries/*.summary; do 
  echo $kind  --  $name  --  $summary 
#  name=`basename $i .summary`
  nb_mutants=`ls -l  mutants/${name}/${name}.mutant*.lus | grep -v kill | wc -l`
  echo "  Nb mutants: $nb_mutants"
  nb_conds=`cat conds/$name.conds | wc -l`
  echo "  Nb conds: $nb_conds"
  nb_tests=`cat tests/$name.test_suite | grep -v xml | wc -l`
  ls tests/$name.test_suite.no_mcdc >/dev/null 2>/dev/null
  no_mcdc=$?
  [ $no_mcdc -eq 0 ] && nb_unmet_mcdc=`cat tests/$name.test_suite.no_mcdc | wc -l`
  echo "  Nb tests: $nb_tests (`[ $no_mcdc -eq 0 ] && echo "no mcdc achieved, $nb_unmet_mcdc conditions unmet"; [ $no_mcdc -ne 0 ] && echo "mcdc achieved"`)"
  killed=`cat $summary | grep "killed by test"| wc -l`
  echo "  Nb mutants killed by TS: $killed/$nb_mutants"
  ls tests/${name}.test_suite_new >/dev/null 2> /dev/null
  exist_new=$?
  new_tests=0
  new_killed=0
  remaining=`cat $summary | grep "unkilled" | wc -l`
  [ $exist_new -eq 0 ] && new_tests=`cat tests/${name}.test_suite_new 2>/dev/null| grep -v xml | wc -l`
  [ $exist_new -eq 0 ] && new_killed=`cat ${summary}_new | grep "killed by new test"| wc -l`
  [ $exist_new -eq 0 ] && remaining=`cat ${summary}_new | grep "Unable"| wc -l`
  echo "  Nb new tests: $new_tests"
  echo "  Nb newly killed mutants: $new_killed"
  echo "  Mutants (total; killed by TS, killed by mutant based tests, remaining): $nb_mutants / $killed / $new_killed / $remaining"
  deadcode=0
  grep -q "dead code" ${summary}_new 
  [ $? -eq 0 ] && deadcode=1
  [ $deadcode -eq 1 ] && echo "  Deadcode detected"
done;
done
