import argparse
import textwrap
import json
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import pylab as P


def _analysis(f):
    ff = open(f, 'r').readlines()
    #csvF = open((f+".csv"), 'w')
    benchWithData = {}
    csv = "benchmark, nb_mutants, nb_conds, nb_tests, nb_killed_mutants, nb_new_tests, nb_new_killed_mutants, nb_still_alive_mutants\n"
    bench = []
    nb_mutants = []
    nb_conds = []
    nb_tests = []
    nb_killed_mutants = []
    nb_new_tests = []
    nb_new_killed_mutants = []
    nb_still_alive_mutants = []
    total_mcdc = 0
    total_no_mcdc = 0
    total_partial_mcdc = 0
    for line in ff:
        try:
            if "--" in line:
                bench.append(line.split("--")[1])
            if "Nb mutants:" in line:
                nb_mutants.append((line.split(":")[1]).rstrip('\n'))
            if "Nb conds" in line:
                nb_conds.append((line.split(":")[1]).rstrip('\n'))
            if "Nb tests" in line:
                r = line.split(":")[1]
                r1 = r.split()
                number_tests = (r1[0]).strip('\n')
                met_conditions = r1[1:]
                m = " ".join(x for x in met_conditions)
                if 'unmet' in m:
                    total_no_mcdc = total_no_mcdc + int(met_conditions[3])
                    total_partial_mcdc += 1
                elif 'no' not in m:
                    total_mcdc += 1
                #print number_tests + " -> " +met_conditions
                nb_tests.append(((line.split(":")[1]).split()[0]).rstrip('\n'))
            if "Nb mutants killed by TS" in line:
                nb_killed_mutants.append((((line.split(":"))[1]).split("/")[0]).rstrip('\n'))
            if "new tests" in line:
                nb_new_tests.append((line.split(":"))[1])
            if "newly" in line:
                nb_new_killed_mutants.append((line.split(":"))[1])
            if "remaining" in line:
                nb_still_alive_mutants.append((line.split(":")[1]).split("/")[3])

            # csv += bench + ", " + nb_mutants + ", " + nb_conds + ", " + nb_tests + ", " + nb_killed_mutants + ", "+ nb_new_tests +", "+\
            #        nb_new_killed_mutants +  ", "+ nb_still_alive_mutants + "\n"
            print bench
        except Exception as e:
            print str(e)
    print "N of benchmarks: " + str(len(bench))

    print "=============MCDC Conditions ============"
    print "N of conditions: " + str(len(nb_conds))
    print "Average number of conditions: " + str(np.mean([int(x) for x in nb_conds]))
    print "Max number of conditions: " + str(np.max([int(x) for x in nb_conds]))
    print "Total number of conditions: " + str(np.sum([int(x) for x in nb_conds]))
    print "Total number of MCDC conditions met " + str(total_mcdc)
    print "Total number of Partial MCDC conditions met " + str(total_partial_mcdc)
    print "Total conditions not passed MCDC " + str(total_no_mcdc)

    print "==========  MUTANTS ================="
    print "N of mutants: " + str(len(nb_mutants))
    print "Total number of mutants: " + str(np.sum([int(x) for x in nb_mutants]))
    print "Total number of tests: " + str(np.sum([int(x) for x in nb_tests]))
    print "N of killed mutants: " + str(np.sum([int(x) for x in nb_killed_mutants]))
    print "\n--- Averages\n"
    print "Average number of tests: " + str(np.mean([int(x) for x in nb_tests]))
    print "Average number of mutants: " + str(np.mean([int(x) for x in nb_mutants]))
    print "Average number of killed mutants: " + str(np.mean([int(x) for x in nb_killed_mutants]))
    print "==========  NEW TESTS================="
    print "Total number of new tests: " + str(np.sum([int(x) for x in nb_new_tests]))
    print "Total number of newly killed mutants: " + str(np.sum([int(x) for x in nb_new_killed_mutants]))
    print "Total number of unkilled mutants: " + str(np.sum([int(x) for x in nb_still_alive_mutants]))
    print "\n Averages ---- \n"
    print "Average number of new tests: " + str(np.mean([int(x) for x in nb_new_tests]))
    print "Average number of newly killed mutants: " + str(np.mean([int(x) for x in nb_new_killed_mutants]))
    print "Average number of unkilled mutants: " + str(np.mean([int(x) for x in nb_still_alive_mutants]))


    print "Plotting .....  "
    fig = plt.figure()
    #h = fig.add_subplot(111)
    p = fig.add_subplot(111)
    #p1 = fig.add_subplot(222)
    # p3 = fig.add_subplot(212)
    # p4 = fig.add_subplot(122)
    #h.hist([int(x) for x in nb_conds], bins=20, histtype='stepfilled', normed=True, color='b', label='N of MCDC conditions')
    #h.hist([int(x) for x in nb_tests], bins=20, histtype='stepfilled', normed=True, color='r', alpha=0.5, label='N of tests generated')
    # h.title("Comparison of MCDC conditions and generated tests")
    # h.xlabel("Value")
    # h.ylabel("Probability")
    # h.legend()
    #p.plot([int(x) for x in nb_conds], [int(x) for x in nb_tests], 'ro', color='blue', lw=2)
    p.plot([int(x) for x in nb_tests], [int(x) for x in nb_killed_mutants], 'rx', color='green', lw=2)
    # p3.plot([int(x) for x in nb_conds], [int(x) for x in nb_tests], 'ro', color='blue', lw=2)
    # p4.plot([int(x) for x in nb_tests], [int(x) for x in nb_killed_mutants], 'rx', color='green', lw=2)
    p.plot([0.1, 1000], [0.1, 1000], 'k-', c='red', lw=2)
    #p1.plot([0.1, 1000], [0.1, 1000], 'k-', c='red', lw=2)
    p.set_yscale('log')
    p.set_xscale('log')
    # p1.set_yscale('log')
    # p1.set_xscale('log')

    plt.savefig('killed_mutants.png')


  # misc -- _6counter -- summaries/_6counter.summary
  # Nb mutants: 182
  # Nb conds: 18
  # Nb tests: 1 (mcdc achieved)
  # Nb mutants killed by TS: 176/182
  # Nb new tests: 2
  # Nb newly killed mutants: 5
  # Mutants (total; killed by TS, killed by mutant based tests, remaining): 182 / 176 / 5 / 1





if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        prog='Analysis',
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description=textwrap.dedent('''\
                  Analysis
            --------------------------------
            '''))
    parser.add_argument('-f', '--file', required=True, dest="file" )
    args = parser.parse_args()
    f = args.file
    try:
        _analysis(f)
    except Exception as e:
        print str(e)
